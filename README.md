# rate-io-mappings
	List of LOB 
	Auto 
	 - private/family passenger
	 - motor cycle  - not mapped yet?
		- Guest passenger coverage  only for motor cycle policy
		- Sample inputs
	 - specialty vehicle
	Property
		○ Renter insurance 
		○ Home owner Insurance 
		○ Condo insurance 
	
	Umbrella Insurance


## Mappings Speadsheet
- https://amfam.sharepoint.com/:x:/r/sites/ClassicMigrationProgram705/_layouts/15/doc2.aspx?sourcedoc=%7BE58F4CEB-93BC-470A-9DDE-8ED89E0F7B54%7D&file=Input%20and%20Output%20of%20SP.xlsx&action=default&mobileredirect=true&clickparams=eyJBcHBOYW1lIjoiVGVhbXMtRGVza3RvcCIsIkFwcFZlcnNpb24iOiIyNy8yMzAzMDUwMTEwNSIsIkhhc0ZlZGVyYXRlZFVzZXIiOmZhbHNlfQ%3D%3D&cid=87ae8d9a-9292-474d-86ea-b0a7e767b2e3


## Rate Adaptar EJB project
- https://gitlab.com/amfament/amfam/RATING/rateadptr
- Coverages Mappings  https://gitlab.com/amfament/amfam/RATING/rateadptr/-/blob/develop/rateadptrMDB/src/main/resources/mapping/CoveragesMapping.xsl
- Common Mappings - https://gitlab.com/amfament/amfam/RATING/rateadptr/-/blob/develop/rateadptrMDB/src/main/resources/mapping/CommonMapping.xsl
- Vehicle mappings - https://gitlab.com/amfament/amfam/RATING/rateadptr/-/blob/develop/rateadptrMDB/src/main/resources/mapping/VehicleMapping.xsl

## Classic Rate Engine Mappings from mainframe code 
- Call rate Module - https://gitlab.com/sxp249/rate-io-mappings/-/blob/main/AFD30000%20-%20Call%20Rate.COB
-  Convert Rate String to RLA - https://gitlab.com/sxp249/rate-io-mappings/-/blob/main/AFD30005%20-%20Convert%20Rate%20String%20to%20RLA.COB
